import React from 'react';
import styled from 'styled-components';
import { ElevatorWorkingState, Floor } from '../models';
import { FaAngleDown, FaAngleUp, FaBars } from 'react-icons/fa';

type ElevatorScreenProps = { elevatorState: ElevatorWorkingState; currentFloor: Floor };

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const OuterContainer = styled(Wrapper)`
  position: absolute;
  top: 0.8em;

  font-size: 0.9em;
  background-color: black;
  padding: 0.4em 0;
  color: green;

  width: 7em;
  height: 1em;
  line-height: 1em;

  .icon {
    position: absolute;
    left: 0.4em;
  }
`;

const FloorNumber = styled.span`
  margin-left: 5px;
`;

export const ElevatorScreen = ({ elevatorState, currentFloor }: ElevatorScreenProps) => {
  const stateIcon =
    elevatorState === 'idle' ? <FaBars/> : elevatorState === 'up' ? <FaAngleUp/> : <FaAngleDown/>;
  return (
    <OuterContainer>
      {stateIcon}
      <FloorNumber> {currentFloor}</FloorNumber>
    </OuterContainer>
  );
};
